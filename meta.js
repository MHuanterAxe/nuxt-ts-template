const fs = require('fs')
const path = require('path')

module.exports = {
  'prompts': {
    'name': {
      'type': 'string',
      'required': true,
      'message': 'Enter the project name',
      'validate': (name) => {
        if (!/^[a-z][a-z0-9_]+[a-z0-9]$/u.test(name)) {
          return `Use lowercase letters, underscores, and numbers only. ` +
            `Note that underscore can not be the last char.`
        }

        return true
      },
    },
    'description': {
      'type': 'string',
      'required': false,
      'message': 'Enter the project description',
      'default': 'Default Vue+Nuxt+Typescript project',
    },
    'author': {
      'type': 'string',
      'required': true,
      'message': 'Enter author name: for example: Name Surname <example@example.com>',
      'default': 'Name Surname <example@example.com>',
    },
  },
  'skipInterpolation': 'src/**/*.vue',
  'complete': (projectData, { logger, chalk }) => {
    const dest = projectData.inPlace ? '.' : projectData.destDirName
    const dotenv = path.join(dest, 'config', '.env')
    const dotenvTemplate = path.join(dest, 'config', '.env.template')

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    if (!fs.existsSync(dotenv)) {
      fs.copyFileSync(dotenvTemplate, dotenv)
      logger.log(chalk.green(`Created new configuration ${dotenv}`))
    }

    logger.log(chalk.blue('Your project is ready for develop!\n'))
    logger.log(chalk.green(`To get started:\n`))
    logger.log(chalk.blue('npm install'))
    logger.log(chalk.blue('npm run dev\n'))

    const url = 'https://gitlab.com/MHuanterAxe/nuxt-ts-template'
    logger.log(`Documentation can be found at`)
    logger.log(chalk.blueBright(url))
    logger.log(chalk.blue('Good luck!'))
  },
}
