import Component from 'nuxt-class-component'
import Vue from 'vue'
import { useStore } from 'vuex-simple'

import TypedStore from '~/logic/store'

@Component({})
/**
 * Mixin to inject `typedStore` field into all components using this mixin.
 */
export default class TypedStoreMixin extends Vue {
  public typedStore: TypedStore = useStore(this.$store)
}
