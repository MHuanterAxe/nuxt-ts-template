import { AxiosInstance } from 'axios'
import { Container } from 'vue-typedi'
import tokens from '~/logic/tokens'

export default class Api {
  /**
   * @returns Global 'axios' instance from the IoC container
   */
  protected get $axios (): AxiosInstance {
    return Container.get(tokens.AXIOS) as AxiosInstance
  }
}
