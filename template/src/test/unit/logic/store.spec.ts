import TypedStore from '@/logic/store'

describe('Store', () => {
  let typedStore: TypedStore

  beforeEach(() => {
    typedStore = new TypedStore()
  })
  it('should have module posts', () => {
    expect(typedStore).toBeTruthy()
  })
})
