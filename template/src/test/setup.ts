import vueTestUtilMatchers from 'jest-matcher-vue-test-utils'

// Installing custom assertions, see:
expect.extend({ ...vueTestUtilMatchers })
