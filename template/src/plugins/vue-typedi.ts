import 'reflect-metadata'
import { AxiosInstance } from 'axios'
import Vue, { VueConstructor } from 'vue'
import VueTypeDI, { Container } from 'vue-typedi'

import tokens from '@/logic/tokens'

export function install (
  vueConstructor: VueConstructor,
  $axios: AxiosInstance,
): void {
  vueConstructor.use(VueTypeDI)
  Container.set(tokens.AXIOS, $axios)
}

export default ({ $axios }: { $axios: AxiosInstance }): void => {
  install(Vue, $axios)
}
