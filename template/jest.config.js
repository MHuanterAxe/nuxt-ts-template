module.exports = {
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '^~/(.*)$': '<rootDir>/src/$1',
    '^vue$': 'vue/dist/vue.common.js'
  },
  moduleFileExtensions: [
    'ts',
    'js',
    'vue',
    'json'
  ],
  transform: {
    '^.+\\.ts$': 'ts-jest',
    '^.+\\.js$': 'babel-jest',
    '.*\\.(vue)$': 'vue-jest'
  },
  collectCoverage: true,
  coverageThreshold: {
    global: { statements: 90 }
  },
  collectCoverageFrom: [
    '<rootDir>/src/logic/**/*.ts'
  ],
  setupFiles: ['jest-localstorage-mock'],
  setupFilesAfterEnv: [
    // Add matchers via `expect.extend()`
    '<rootDir>/src/test/setup.ts'
  ]
}
