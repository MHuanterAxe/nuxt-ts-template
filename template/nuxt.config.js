import { resolve } from 'path'
import { config as dotenv } from 'dotenv'

import pkg from './package.json'

const ROOT_DIR = resolve(__dirname)
const SRC_DIR = 'src'
const ENV_PATH = resolve(ROOT_DIR,'config', '.env')

dotenv({
  path: ENV_PATH
})

const isDev = process.env.NODE_ENV === 'development'

export default {
  /**
   * html headers of the page
   */
  head: {
    title: pkg.name,
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /**
   * Specify Nuxt source directory
   */
  srcDir: SRC_DIR,
  rootDir: ROOT_DIR,

  /**
   * Specify global css files
   *
   * Add additional styles here
   */
  css: [
    '@/assets/style.scss'
  ],

  /**
   * Specify Nuxt plugins for your project
   */
  plugins: [
    '@/plugins/vue-typedi.ts'
  ],

  /**
   * Auto import components
   *
   * @default true
   */
  components: true,

  /**
   * Modules that are used in build-time only.
   *
   * @link https://go.nuxtjs.dev/config-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/stylelint-module'
  ],

  /**
   * Extra Nuxt modules.
   *
   * @Link https://go.nuxtjs.dev/config-modules
   */
  modules: [
    '@nuxtjs/axios'
  ],

  /**
   * Axios global configuration
   */
  axios: {
    debug: isDev,
    https: isDev,
    proxyHeadersIgnore: ['accept', 'accept-encoding', 'host'],
    baseURL: process.env.API_URL
  },

  /**
   * Build configuration
   */
  build: {

  }
}
